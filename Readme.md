# E-LOG Clicker

## Objectif

The objective is to allow a continuous click without having to hold the mouse button and to be able to activate / deactivate the function easily.

## Compile

Compile option (Link setting) -lwinmm  

Output file is bin\Debug\E-LOG-Clicker.exe with size 76.64 KB
 

## How to use

1. Download the file `ELOG-Clicker` https://gitlab.com/danycor/elog-clicker/-/archive/master/elog-clicker-master.zip
2. Run the `ELOG-Clicker.exe` by double-clicking on it
3. **Press `F12`** key to **enable** the continuous left click (you must hear a sound)
4. **Press `F12`** minimum 200ms after enable to **disable** continuous left click (you must hear a sound) or just **left click** on the screen
5. **Press `F6`** key to **enable** the continuous right click (you must hear a sound)
6. **Press `F6`** minimum 200ms after enable to **disable** continuous right click (you must hear a sound) or just **right click** on the screen