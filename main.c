#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <windows.h>
#include <mmsystem.h>
#include <w32api.h>

void clic(int button) {
    PlaySoundA((LPCSTR) "./clic.WAV", NULL, SND_FILENAME | SND_ASYNC);
    mouse_event(button, 0, 0, 0, 0);
}

void releaseClic(int button) {
    PlaySoundA((LPCSTR) "./release.WAV", NULL, SND_FILENAME | SND_ASYNC);
    mouse_event(button, 0, 0, 0, 0);
}


int main()
{
    clock_t lastEvent = clock();
    int isClicLeft = 0;
    int isClicRight = 0;
    while (1)
    {
        short keyF12 = (short)GetKeyState(0x7B);
        short keyF6 = (short)GetKeyState(0x75);

        // Left
        if (keyF12<0 && lastEvent < clock() - 200) {
            lastEvent = clock();
            if(isClicLeft == 1 && GetKeyState(0x01) < 0) {
                releaseClic(0x0004);
                isClicLeft = 0;
                printf("Release Left\n");
            } else {
                clic(0x0002);
                isClicLeft = 1;
                printf("Clic Left\n");
            }
        }

        // Right
        if (keyF6<0 && lastEvent < clock() - 200) {
            lastEvent = clock();
            if(isClicRight == 1 && GetKeyState(0x02) < 0) {
                releaseClic(0x0010);
                isClicRight = 0;
                printf("Release Right\n");
            } else {
                clic(0x0008);
                isClicRight = 1;
                printf("Clic Right\n");
            }
        }
        usleep(20000);
    }
    return 0;
}
